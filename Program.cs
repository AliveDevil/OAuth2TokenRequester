﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Text.Encodings.Web;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;

namespace OAuth2TokenRequester
{
    class Program
    {
        private const string INDENT_STRING = "\t";

        static string FormatJson(string json)
        {

            int indentation = 0;
            int quoteCount = 0;
            var result =
                from ch in json
                let quotes = ch == '"' ? quoteCount++ : quoteCount
                let lineBreak = ch == ',' && quotes % 2 == 0 ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, indentation)) : null
                let openChar = ch == '{' || ch == '[' ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, ++indentation)) : ch.ToString()
                let closeChar = ch == '}' || ch == ']' ? Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, --indentation)) + ch : ch.ToString()
                select lineBreak == null
                            ? openChar.Length > 1
                                ? openChar
                                : closeChar
                            : lineBreak;

            return String.Concat(result);
        }

        static int Main(string[] args)
        {
            KeyValuePair<string, string> kvp(string k, string v) => new KeyValuePair<string, string>(k, v);

            var app = new CommandLineApplication();

            app.Command("oauth", oauth =>
            {
                var configOption = oauth.Option("-c|--config", "Configuration File", CommandOptionType.MultipleValue);
                var hostOption = oauth.Option("--host", "Host", CommandOptionType.SingleValue);
                var clientIdOption = oauth.Option("-id|--clientid", "Client ID", CommandOptionType.SingleValue);
                var redirectOption = oauth.Option("-r|--redirect", "Redirect URI", CommandOptionType.SingleValue);
                var scopesOption = oauth.Option("-s|--scope", "Scopes", CommandOptionType.MultipleValue);

                IEnumerable<KeyValuePair<string, string>> GetOAuthArgumentValues()
                {
                    if (hostOption.HasValue()) yield return kvp("host", hostOption.Value());
                    if (clientIdOption.HasValue()) yield return kvp("clientId", clientIdOption.Value());
                    if (redirectOption.HasValue()) yield return kvp("redirectUri", redirectOption.Value());
                    if (scopesOption.HasValue())
                    {
                        var values = scopesOption.Values;
                        int count = values.Count;
                        for (int i = 0; i < count; i++)
                            yield return kvp($"scopes:{i}", values[i]);
                    }
                }

                oauth.Command("auth", auth =>
                {
                    var authPartOption = auth.Option("-a|--auth", "Auth Part for URI", CommandOptionType.SingleValue);

                    IEnumerable<KeyValuePair<string, string>> GetAuthArgumentValues()
                    {
                        if (authPartOption.HasValue()) yield return kvp("auth", authPartOption.Value());
                    }

                    auth.OnExecute(async () =>
                    {
                        var builder = new ConfigurationBuilder();
                        builder.AddJsonFile("OAuth2TokenRequester.json", true);
                        builder.AddInMemoryCollection(GetOAuthArgumentValues());
                        builder.AddInMemoryCollection(GetAuthArgumentValues());
                        configOption.Values.ForEach(f => builder.AddJsonFile(f));
                        var configuration = builder.Build();

                        var host = configuration.GetSection("host").Value;
                        var authPart = configuration.GetSection("authPart").Value;
                        var clientId = configuration.GetSection("clientId").Value;
                        var redirectUri = UrlEncoder.Default.Encode(configuration.GetSection("redirectUri").Value);
                        var scopes = UrlEncoder.Default.Encode(string.Join(" ", configuration.GetSection("scopes").GetChildren().Select(x => x.Value)));
                        Random random = new Random();
                        var state = random.Next();

                        var authUri = new Uri($"{host}{authPart}?client_id={clientId}&response_type=code&redirect_uri={redirectUri}&scope={scopes}&state={state}");
                        Console.WriteLine("Open following URL in browser and copy retrieved code to \"response.json\"");
                        Console.WriteLine(authUri);
                        Console.WriteLine("Then execute following command: OAuth2TokenRequester oauth token <your arguments> -c response.json");

                        using (var writer = File.CreateText("response.json"))
                            await writer.WriteLineAsync($"{{\n\t\"state\": {state},\n\t\"code\": \"\"\n}}");

                        return 0;
                    });


                    auth.HelpOption("-?|-h|--help");
                });
                oauth.Command("token", token =>
                {
                    var tokenPartOption = token.Option("--token", "Token Part for URI", CommandOptionType.SingleValue);
                    var clientSecretOption = token.Option("-secret|--clientsecret", "Client Secret", CommandOptionType.SingleValue);
                    var stateOption = token.Option("--state", "State Value", CommandOptionType.SingleValue);
                    var responseOption = token.Option("--code", "Response code", CommandOptionType.SingleValue);

                    IEnumerable<KeyValuePair<string, string>> GetTokenArgumentValues()
                    {
                        if (tokenPartOption.HasValue()) yield return kvp("token", tokenPartOption.Value());
                        if (clientSecretOption.HasValue()) yield return kvp("clientSecret", clientSecretOption.Value());
                        if (responseOption.HasValue()) yield return kvp("code", responseOption.Value());
                        if (stateOption.HasValue()) yield return kvp("state", stateOption.Value());
                    }

                    token.OnExecute(async () =>
                    {
                        var builder = new ConfigurationBuilder();
                        builder.AddJsonFile("OAuth2TokenRequester.json", true);
                        builder.AddInMemoryCollection(GetOAuthArgumentValues());
                        builder.AddInMemoryCollection(GetTokenArgumentValues());
                        configOption.Values.ForEach(f => builder.AddJsonFile(f));
                        var configuration = builder.Build();

                        var host = configuration.GetSection("host").Value;
                        var tokenPart = configuration.GetSection("tokenPart").Value;
                        var clientId = configuration.GetSection("clientId").Value;
                        var clientSecret = UrlEncoder.Default.Encode(configuration.GetSection("clientSecret").Value);
                        var redirectUri = UrlEncoder.Default.Encode(configuration.GetSection("redirectUri").Value);
                        var scopes = UrlEncoder.Default.Encode(string.Join(" ", configuration.GetSection("scopes").GetChildren().Select(x => x.Value)));
                        var code = UrlEncoder.Default.Encode(configuration.GetSection("code").Value);
                        var state = UrlEncoder.Default.Encode(configuration.GetSection("state").Value);

                        var tokenUri = new Uri($"{host}{tokenPart}");
                        using (var client = new HttpClient())
                        {
                            var response = await client.PostAsync(
                                tokenUri,
                                new StringContent(
                                    $"grant_type=authorization_code&client_id={clientId}&client_secret={clientSecret}&redirect_uri={redirectUri}&scope={scopes}&code={code}&state={state}",
                                    Encoding.UTF8,
                                    "application/x-www-form-urlencoded"));

                            var content = FormatJson(await response.Content.ReadAsStringAsync());

                            using (var writer = File.CreateText("token.json"))
                                await writer.WriteLineAsync(content);

                            Console.WriteLine(content);
                        }

                        return 0;
                    });


                    token.HelpOption("-?|-h|--help");
                });
                oauth.HelpOption("-?|-h|--help");

                oauth.OnExecute(() =>
                {
                    oauth.ShowHelp();

                    return 0;
                });
            });

            app.HelpOption("-?|-h|--help");

            app.OnExecute(() =>
            {
                app.ShowHelp();

                return 0;
            });

            return app.Execute(args);
        }
    }

    class OAuthAccess
    {
        public string Token_Type { get; set; }
        public int Expires_In { get; set; }

        public string Scope { get; set; }

        public string Access_Token { get; set; }

        public string Refresh_Token { get; set; }
    }
}
